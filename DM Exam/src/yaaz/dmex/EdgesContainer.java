package yaaz.dmex;

import java.util.ArrayList;
import yaaz.dmex.Graph.Edge;


class EdgesContainer {
	
	
	protected final ArrayList<Edge> edges=new ArrayList<>();
	
	
	
	public int getEdgesNumber() {
		return edges.size();
	}
	public Edge getEdge(int i) {
		return edges.get(i);
	}
	public Iterable<Edge> getEdges() {
		return edges::iterator;
	}
	
	
}