package yaaz.dmex;

import java.util.ArrayList;
import java.util.Iterator;

public class Graph extends EdgesContainer {
	
	
	protected final VerticesList vertices=new VerticesList();
	
	
	
	public int getVerticesNumber() {
		return vertices.size();
	}
	public Vertex getVertex(int i) {
		return vertices.get(i);
	}
	public Iterable<Vertex> getVertices() {
		return vertices::iterator;
	}
	public void removeVertex(int i) {
		vertices.remove(i);
	}
	public Graph[] splitIntoConnected() {
		if(vertices.size()==0) return new Graph[0];
		boolean[] arr1=new boolean[vertices.size()], arr2=new boolean[vertices.size()];
		int[] map=new int[vertices.size()];
		int visited=widthSearch(arr1, 0);
		if(visited==vertices.size()) return new Graph[] {this};
		ArrayList<Graph> result=new ArrayList<>();
		result.add(copyConnectedPart(map, arr1, arr2));
		for(int i=0;i<map.length;i++) arr2[i]=arr1[i];
		while(visited<vertices.size()) {
			int start=0;
			for(start=0;start<map.length;start++) {
				if(!arr1[start]) break;
			}
			visited+=widthSearch(arr1, start);
			result.add(copyConnectedPart(map, arr1, arr2));
			for(int i=0;i<map.length;i++) arr2[i]=arr1[i];
		}
		return result.toArray(new Graph[result.size()]);
	}
	private Graph copyConnectedPart(int[] map, boolean[] arr1, boolean[] arr2) {
		Graph g=new Graph();
		for(int i=0;i<map.length;i++) {
			if(arr1[i]&&!arr2[i]) {
				Vertex ov=getVertex(i);
				Vertex v=ov.copy(g);
				map[i]=v.getIndex();
				for(Edge e : ov.edges) {
					Vertex n=ov.getNeighbor(e);
					if(ov.getIndex()>n.getIndex()) {
						g.new Edge(g.getVertex(map[e.a.getIndex()]), g.getVertex(map[e.b.getIndex()]), e.weight);
					}
				}
			}
		}
		return g;
	}
	public boolean isConnected() {
		if(vertices.size()==0) return false;
		if(vertices.size()<=1000) return isConnectedDepth();
		return isConnectedWidth();
	}
	private boolean isConnectedWidth() {
		return widthSearch(new boolean[vertices.size()], 0)==vertices.size();
	}
	private int widthSearch(boolean[] visited, int start) {
		ArrayList<Vertex> a=new ArrayList<>(), b=new ArrayList<>();
		a.add(getVertex(start));
		visited[start]=true;
		int vs=1;
		while(!a.isEmpty()) {
			for(Vertex v : a) {
				for(Vertex n : v.getNeighbors()) {
					if(!visited[n.getIndex()]) {
						visited[n.getIndex()]=true;
						vs++;
						b.add(n);
					}
				}
			}
			a.clear();
			ArrayList<Vertex> t=a;
			a=b;
			b=t;
		}
		return vs;
	}
	private boolean isConnectedDepth() {
		boolean[] visited=new boolean[vertices.size()];
		int vs=isConnectedDepthVisit(vertices.get(0), visited);
		return vs==visited.length;
	}
	private int isConnectedDepthVisit(Vertex v, boolean[] visited) {
		if(visited[v.getIndex()]) return 0;
		visited[v.getIndex()]=true;
		int c=1;
		for(Vertex t : v.getNeighbors()) c+=isConnectedDepthVisit(t, visited);
		return c;
	}
	
	
	
	
	
	public class Vertex extends EdgesContainer {
		
		
		protected int index;
		
		
		public Vertex() {
			index=Graph.this.getVerticesNumber();
			Graph.this.vertices.add(this);
		}
		
		
		public boolean isRemoved() {
			return index==-1;
		}
		
		
		public int getIndex() {
			return index;
		}
		
		
		public Iterable<Vertex> getNeighbors() {
			return ()->new NeighborIterator(this);
		}
		
		
		public Vertex getNeighbor(Edge e) {
			if(e.a==this) return e.b;
			if(e.b==this) return e.a;
			throw new IllegalArgumentException("Edge is not connected to this vertex");
		}
		public Edge getEdge(Vertex v) {
			for(Edge e : edges) {
				if(getNeighbor(e)==v) return e;
			}
			return null;
		}
		
		
		public void remove() {
			Graph.this.vertices.remove(index);
		}
		
		
		public Graph getGraph() {
			return Graph.this;
		}
		
		
		protected Vertex copy(Graph g) {
			return g.new Vertex();
		}
		
		
	}
	public class NamedVertex extends Vertex {
		protected final String name;
		public NamedVertex(String n) {name=n;}
		public String getName() {return name;}
		public String toString() {return "Vertex-"+name;}
		protected NamedVertex copy(Graph g) {
			return g.new NamedVertex(name);
		}
	}
	public class IndexedVertex extends Vertex {
		protected final int index;
		public IndexedVertex(int i) {index=i;}
		public int getIndex() {return index;}
		public String toString() {return "Vertex-"+index;}
		protected IndexedVertex copy(Graph g) {
			return g.new IndexedVertex(index);
		}
	}
	public class PointVertex extends Vertex {
		protected final int x, y;
		public PointVertex(int x, int y) {this.x=x;this.y=y;}
		public int getX() {return x;}
		public int getY() {return y;}
		public int getDistanceTo(PointVertex v) {return (int) Math.sqrt(Math.pow(x-v.x, 2)+Math.pow(y-v.y, 2));}
		public String toString() {return "Vertex("+x+", "+y+")";}
		protected PointVertex copy(Graph g) {
			return g.new PointVertex(x, y);
		}
	}
	
	
	
	
	
	public class Edge {
		
		
		public final Graph.Vertex a, b;
		public final long weight;
		
		
		
		public Edge(int a, int b) {
			this(a, b, 0);
		}
		public Edge(int a, int b, long w) {
			this(Graph.this.getVertex(a), Graph.this.getVertex(b), w);
		}
		public Edge(Graph.Vertex a, Graph.Vertex b) {
			this(a, b, 0);
		}
		public Edge(Graph.Vertex a, Graph.Vertex b, long w) {
			if(a==null||b==null) throw new NullPointerException();
			if(getGraph()!=a.getGraph()||getGraph()!=b.getGraph()) throw new IllegalArgumentException("Vertices and edge should be from one graph");
			if(a.isRemoved()||b.isRemoved()) throw new IllegalArgumentException("Vertex was removed from graph");
			this.a=a;
			this.b=b;
			weight=w;
			a.edges.add(this);
			b.edges.add(this);
			getGraph().edges.add(this);
		}
		
		
		public void remove() {
			getGraph().edges.remove(this);
			if(!a.isRemoved()) a.edges.remove(this);
			if(!b.isRemoved()) b.edges.remove(this);
		}
		
		
		public Graph getGraph() {
			return Graph.this;
		}
		
		
		public String toString() {
			return "Edge "+a.toString()+" -("+weight+")- "+b.toString();
		}
		
		
	}
	
	
	
	
	
	private static class NeighborIterator implements Iterator<Vertex> {
		private final Vertex vertex;
		private final Iterator<Edge> itr;
		NeighborIterator(Vertex v) {
			vertex=v;
			itr=v.edges.iterator();
		}
		public boolean hasNext() {return itr.hasNext();}
		public Vertex next() {return vertex.getNeighbor(itr.next());}
	}
	
	
}
