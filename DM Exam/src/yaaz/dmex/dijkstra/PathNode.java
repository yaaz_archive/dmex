package yaaz.dmex.dijkstra;

import yaaz.dmex.Graph;


public class PathNode {
	
	
	public final Graph.Vertex vertex;
	public final Graph.Edge path;
	public final long pathLength;
	
	
	protected PathNode(Graph.Vertex v, Graph.Edge p, long l) {
		vertex=v;
		path=p;
		pathLength=l;
	}
	
	
	public Graph.Vertex getNextNode() {
		if(path==null) return null;
		return vertex.getNeighbor(path);
	}
	
	
	public String toString() {
		if(path==null) return "OriginPathNode "+vertex.toString();
		return "PathNode "+vertex.toString()+" -("+path.weight+")-> "+getNextNode().toString()+" pathLength="+pathLength;
	}
	
	
}
