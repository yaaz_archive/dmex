package yaaz.dmex.dijkstra;

import yaaz.dmex.Graph;
import yaaz.dmex.util.AlgorithmBase;


class Edge extends AlgorithmBase.Edge<Vertex, Edge> {
	
	
	protected Edge(Graph.Edge o, Vertex a, Vertex b) {
		super(o, a, b);
	}
	
	
}
