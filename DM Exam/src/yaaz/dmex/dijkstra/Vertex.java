package yaaz.dmex.dijkstra;

import yaaz.dmex.Graph;
import yaaz.dmex.util.AlgorithmBase;


class Vertex extends AlgorithmBase.Vertex<Vertex, Edge> {
	
	
	protected volatile Edge path;
	protected volatile long pathLength;
	protected volatile boolean processed;
	
	
	
	protected Vertex(Graph.Vertex o) {
		super(o);
	}
	
	
}
