package yaaz.dmex.dijkstra;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import yaaz.dmex.Graph;
import yaaz.dmex.util.GraphCopier;


public class Dijkstra {
	
	
	protected Core manualCore;
	protected final Vertex[] vertices;
	protected int verticesProcessed;
	protected boolean finished;
	protected final PathNode[] result;
	
	
	
	public Dijkstra(Graph graph, int vertex) {
		this(graph, vertex, null);
	}
	public Dijkstra(Graph graph, int vertex, ExecutorService executor) {
		this(graph, vertex, executor, Runtime.getRuntime().availableProcessors());
	}
	public Dijkstra(Graph graph, int vertex, ExecutorService executor, int tasksNumber) {
		vertices=new Vertex[graph.getVerticesNumber()];
		result=new PathNode[vertices.length];
		//Copy graph
		GraphCopier<Vertex> copier=new GraphCopier<Vertex>(graph, vertices);
		if(executor==null) copier.copyVertices(Vertex::new).copyEdges(Edge::new);//Single thread
		else copier.copyVertices(Vertex::new, executor, tasksNumber).copyEdges(Edge::new, executor, tasksNumber);//Parallel
		addToResult(vertices[vertex]);
	}
	
	
	
	public boolean isFinished() {
		return finished;
	}
	
	
	public PathNode[] getRawResult() {
		return result;
	}
	
	
	
	public boolean step() {
		if(isFinished()) return false;
		if(manualCore==null) manualCore=new Core(0, vertices.length);
		return addToResult(manualCore.findClosest());
	}
	
	
	/**
	 * Runs the algorithm in current thread
	 */
	public PathNode[] execute() {
		if(!isFinished()) {
			while(step());
		}
		return result;
	}
	
	
	/**
	 * Runs the algorithm with given executor in number of tasks equal to the number of available processors without waiting for the completion
	 * @see Execution#join()
	 */
	public Execution start(ExecutorService executor) {
		return start(executor, Runtime.getRuntime().availableProcessors());
	}
	/**
	 * Runs the algorithm with given executor in given number of tasks without waiting for the completion
	 * @see Execution#join()
	 */
	public Execution start(ExecutorService executor, int tasks) {
		return new Execution(executor, tasks);
	}
	/**
	 * Runs the algorithm with given executor in number of tasks equal to the number of available processors and waits for the completion
	 * @see Execution#join()
	 */
	public PathNode[] execute(ExecutorService executor) {
		return execute(executor, Runtime.getRuntime().availableProcessors());
	}
	/**
	 * Runs the algorithm with given executor in given number of tasks and waits for the completion
	 * @see Execution#join()
	 */
	public PathNode[] execute(ExecutorService executor, int tasks) {
		Core[] cores=new Core[tasks];
		@SuppressWarnings("unchecked")
		Future<Vertex>[] returns=new Future[tasks];
		{
			int step=vertices.length/tasks+1;
			for(int i=0;i<tasks;i++) {
				final int from=i*step;
				final int to=from+step>vertices.length?vertices.length:from+step;
				cores[i]=new Core(from, to);
			}
		}
		while(!isFinished()) {
			for(int i=0;i<tasks;i++) returns[i]=executor.submit(cores[i]);
			try {
				Vertex closest=null;
				for(int i=0;i<tasks;i++) {
					Vertex v=returns[i].get();
					if(closest==null||(v!=null&&v.pathLength<closest.pathLength)) closest=v;
				}
				addToResult(closest);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	
	protected boolean addToResult(Vertex v) {
		if(v==null) {
			finished=true;
			return false;
		}
		v.processed=true;
		verticesProcessed++;
		result[v.origin.getIndex()]=new PathNode(v.origin, v.path==null?null:v.path.origin, v.pathLength);
		if(verticesProcessed==vertices.length) finished=true;
		return true;
	}
	
	
	
	
	
	protected class Core implements Callable<Vertex> {
		protected final int from, to;
		protected Core(int f, int t) {
			from=f;
			to=t;
		}
		protected Vertex findClosest() {
			Vertex result=null;
			for(int i=from;i<to;i++) {
				Vertex v=vertices[i];
				if(v.processed) continue;
				Edge closest=null;
				long len=0;
				for(Edge e : v.edges) {
					Vertex n=v.getNeighbor(e);
					if(n.processed) {
						if(closest==null||n.pathLength+e.getWeight()<len) {
							closest=e;
							len=n.pathLength+e.getWeight();
						}
					}
				}
				if(closest!=null) {
					v.path=closest;
					v.pathLength=len;
					if(result==null||result.pathLength>len) result=v;
				}
			}
			return result;
		}
		public Vertex call() {
			return findClosest();
		}
	}
	
	
	
	
	
	/**
	 * This class represents execution of the algorithm
	 */
	public class Execution {
		private final Thread thread;
		private Execution(final ExecutorService executor, final int tasks) {
			thread=new Thread(()->execute(executor, tasks));
			thread.start();
		}
		public boolean isDone() {
			return !thread.isAlive();
		}
		/**
		 * Waits for algorithm completion
		 */
		public PathNode[] join() {
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return result;
		}
	}
	
	
}
