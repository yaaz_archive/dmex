package yaaz.dmex.boruvka;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import yaaz.dmex.Graph;
import yaaz.dmex.util.GraphCopier;


public class Boruvka {
	
	
	protected Worker manualWorker;
	protected final int vertsNum;
	protected final ConcurrentLinkedQueue<Vertex> vertices;
	protected final AtomicIntegerArray pool;
	protected final AtomicInteger searchCounter, resultArrayPointer;
	protected final Graph.Edge[] result;
	
	
	
	public Boruvka(Graph graph) {
		this(graph, null);
	}
	public Boruvka(Graph graph, ExecutorService executor) {
		this(graph, executor, Runtime.getRuntime().availableProcessors());
	}
	public Boruvka(Graph graph, ExecutorService executor, int tasksNumber) {
		if(executor==null&&!graph.isConnected()) throw new IllegalArgumentException("Graph is not connected");
		searchCounter=new AtomicInteger();
		resultArrayPointer=new AtomicInteger();
		vertsNum=graph.getVerticesNumber();
		result=new Graph.Edge[vertsNum-1];
		pool=new AtomicIntegerArray(vertsNum);
		vertices=new ConcurrentLinkedQueue<>();
		//Copy graph
		if(executor==null) {//Single thread
			new GraphCopier<Vertex>(graph).copyVertices(Vertex::new, vertices).copyEdges(Edge::new);
		}
		else {//Parallel
			Future<Boolean> graphConnected=executor.submit(graph::isConnected);
			GraphCopier<Vertex> copier=new GraphCopier<>(graph);
			copier.copyVertices(Vertex::new, executor, tasksNumber, vertices);
			if(graphConnected.isDone()) {
				try {
					if(!graphConnected.get()) throw new IllegalArgumentException("Graph is not connected");
				} catch (InterruptedException | ExecutionException e) {e.printStackTrace();}
			}
			copier.copyEdges(Edge::new, executor, tasksNumber);
			try {
				if(!graphConnected.get()) throw new IllegalArgumentException("Graph is not connected");
			} catch (InterruptedException | ExecutionException e) {e.printStackTrace();}
		}
	}
	
	
	
	public boolean isFinished() {
		return resultArrayPointer.get()>=vertsNum-1;
	}
	
	
	public Graph.Edge[] getRawResult() {
		return result;
	}
	
	
	public boolean step() {
		if(manualWorker==null) manualWorker=new Worker();
		return manualWorker.step();
	}
	
	
	/**
	 * Runs the algorithm in current thread
	 */
	public Graph.Edge[] execute() {
		if(!isFinished()) new Worker().run();
		return result;
	}
	
	
	/**
	 * Runs the algorithm with given executor in number of tasks equal to the number of available processors without waiting for the completion
	 * @see #join()
	 */
	public Execution start(ExecutorService executor) {
		return start(executor, Runtime.getRuntime().availableProcessors());
	}
	/**
	 * Runs the algorithm with given executor in given number of tasks without waiting for the completion
	 * @see #join()
	 */
	public Execution start(ExecutorService executor, int tasks) {
		Future<?>[] ts=new Future<?>[tasks];
		for(int i=0;i<ts.length;i++) ts[i]=executor.submit(new Worker());
		return new Execution(ts);
	}
	
	
	
	protected void process(Vertex v) {
		Edge e=findMinEdge(v, searchCounter.getAndIncrement());
		if(e!=null) {
			Vertex m=Vertex.merge(e.a, e.b);
			if(m!=null) {
				vertices.remove(m);
				pool.set(m.origin.getIndex(), -1);
				result[resultArrayPointer.getAndIncrement()]=e.origin;
			}
		}
	}
	
	
	private Edge findMinEdge(Vertex v, int searchId) {
		if(v.searchId==searchId) return null;
		v.searchId=searchId;
		Edge min=null, nmin=null;
		for(Edge e : v.edges) {
			if(e.a.getParent()==e.b.getParent()) {
				Edge t=findMinEdge(v.getNeighbor(e), searchId);
				if(t!=null&&(nmin==null||t.getWeight()<nmin.getWeight())) nmin=t;
			}
			else if(min==null||e.getWeight()<min.getWeight()) min=e;
		}
		if(min==null||(nmin!=null&&nmin.getWeight()<min.getWeight())) min=nmin;
		return min;
	}
	
	
	
	
	
	/**
	 * This class represents execution of the algorithm
	 */
	public class Execution {
		private final Future<?>[] tasks;
		private Execution(Future<?>[] t) {tasks=t;}
		public boolean isDone() {
			for(int i=0;i<tasks.length;i++) {
				if(!tasks[i].isDone()) return false;
			}
			return true;
		}
		/**
		 * Waits for algorithm completion
		 */
		public Graph.Edge[] join() {
			for(int i=0;i<tasks.length;i++) {
				try {
					tasks[i].get();
				} catch(InterruptedException | ExecutionException e) {
					throw new RuntimeException(e);
				}
			}
			return result;
		}
	}
	
	
	
	
	
	/**
	 * Working unit
	 */
	protected class Worker implements Runnable {
		protected Iterator<Vertex> iterator=vertices.iterator();
		protected Worker() {}
		protected boolean step() {
			if(isFinished()) return false;
			if(!iterator.hasNext()) iterator=vertices.iterator();
			if(iterator.hasNext()) {
				Vertex v=iterator.next();
				if(v==v.getParent()&&pool.compareAndSet(v.origin.getIndex(), 0, 1)) {
					process(v);
					pool.compareAndSet(v.origin.getIndex(), 1, 0);
				}
				return true;
			}
			return false;
		}
		public void run() {
			while(step());
		}
	}
	
	
}
