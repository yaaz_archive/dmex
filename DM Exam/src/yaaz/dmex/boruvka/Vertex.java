package yaaz.dmex.boruvka;

import java.util.concurrent.atomic.AtomicReference;

import yaaz.dmex.Graph;
import yaaz.dmex.util.AlgorithmBase;


class Vertex extends AlgorithmBase.Vertex<Vertex, Edge> {
	
	
	protected final AtomicReference<Vertex> parent=new AtomicReference<>(this);
	protected volatile int searchId=-1;
	
	
	
	protected Vertex(Graph.Vertex o) {
		super(o);
	}
	
	
	protected Vertex getParent() {
		Vertex p=parent.get();
		if(p==this) return p;
		for(;;) {
			Vertex f=p.getParent();
			if(p==f) return p;
			if(parent.compareAndSet(p, f)) return f;
			p=parent.get();
		}
	}
	
	
	protected static Vertex merge(Vertex a, Vertex b) {
		for(;;) {
			a=a.getParent();b=b.getParent();
			if(a==b) return null;
			if(a.origin.getIndex()>b.origin.getIndex()) {Vertex t=a;a=b;b=t;}
			if(a.parent.compareAndSet(a, b)) return a;
		}
	}
	
	
}
