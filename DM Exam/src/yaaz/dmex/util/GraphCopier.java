package yaaz.dmex.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import yaaz.dmex.Graph;


public class GraphCopier<VERTEX_TYPE extends AlgorithmBase.Vertex<?,?>> {
	
	
	private final Graph graph;
	private final VERTEX_TYPE[] mappingVertices;
	
	
	
	@SuppressWarnings("unchecked")
	public GraphCopier(Graph g) {
		graph=g;
		mappingVertices=(VERTEX_TYPE[]) new AlgorithmBase.Vertex<?,?>[graph.getVerticesNumber()];
	}
	public GraphCopier(Graph g, VERTEX_TYPE[] mappingArray) {
		graph=g;
		if(mappingArray.length<graph.getVerticesNumber()) throw new IllegalArgumentException("Mapping array size should be not less than number of vertices");
		mappingVertices=mappingArray;
	}
	
	
	
	public GraphCopier<VERTEX_TYPE> copyVertices(VertexCopier<VERTEX_TYPE> vertexCopier) {
		return copyVertices(vertexCopier, null);
	}
	public GraphCopier<VERTEX_TYPE> copyVertices(VertexCopier<VERTEX_TYPE> vertexCopier, Collection<VERTEX_TYPE> addVertices) {
		for(Graph.Vertex v : graph.getVertices()) {
			VERTEX_TYPE nv=vertexCopier.copy(v);
			mappingVertices[v.getIndex()]=nv;
			if(addVertices!=null) addVertices.add(nv);
		}
		return this;
	}
	public <EDGE_TYPE extends AlgorithmBase.Edge<?,?>> GraphCopier<VERTEX_TYPE> copyEdges(EdgeCopier<VERTEX_TYPE, EDGE_TYPE> edgeCopier) {
		return copyEdges(edgeCopier, null);
	}
	public <EDGE_TYPE extends AlgorithmBase.Edge<?,?>> GraphCopier<VERTEX_TYPE> copyEdges(EdgeCopier<VERTEX_TYPE, EDGE_TYPE> edgeCopier, Collection<EDGE_TYPE> addEdges) {
		for(Graph.Edge e : graph.getEdges()) {
			EDGE_TYPE ne=edgeCopier.copy(e, (VERTEX_TYPE)mappingVertices[e.a.getIndex()], (VERTEX_TYPE)mappingVertices[e.b.getIndex()]);
			if(addEdges!=null) addEdges.add(ne);
		}
		return this;
	}
	
	
	
	public GraphCopier<VERTEX_TYPE> copyVertices(VertexCopier<VERTEX_TYPE> vertexCopier, ExecutorService executor, int tasksNumber) {
		return copyVertices(vertexCopier, executor, tasksNumber, null);
	}
	public GraphCopier<VERTEX_TYPE> copyVertices(VertexCopier<VERTEX_TYPE> vertexCopier, ExecutorService executor, int tasksNumber, Collection<VERTEX_TYPE> addVertices) {
		final int vertexStep=graph.getVerticesNumber()/tasksNumber+1;
		ArrayList<Future<?>> tasks=new ArrayList<>();
		for(int i=0;i<graph.getVerticesNumber();i+=vertexStep) {
			final int from=i;
			final int to=from+vertexStep>graph.getVerticesNumber()?graph.getVerticesNumber():from+vertexStep;
			tasks.add(executor.submit(()->{
				for(int j=from;j<to;j++) {
					Graph.Vertex v=graph.getVertex(j);
					VERTEX_TYPE nv=vertexCopier.copy(v);
					mappingVertices[v.getIndex()]=nv;
					if(addVertices!=null) addVertices.add(nv);
				}
			}));
		}
		for(Future<?> t : tasks) {
			try {t.get();} catch (InterruptedException | ExecutionException e) {e.printStackTrace();}
		}
		return this;
	}
	public <EDGE_TYPE extends AlgorithmBase.Edge<?,?>> GraphCopier<VERTEX_TYPE> copyEdges(EdgeCopier<VERTEX_TYPE, EDGE_TYPE> edgeCopier, ExecutorService executor, int tasksNumber) {
		return copyEdges(edgeCopier, executor, tasksNumber, null);
	}
	public <EDGE_TYPE extends AlgorithmBase.Edge<?,?>> GraphCopier<VERTEX_TYPE> copyEdges(EdgeCopier<VERTEX_TYPE, EDGE_TYPE> edgeCopier, ExecutorService executor, int tasksNumber, Collection<EDGE_TYPE> addEdges) {
		final int edgeStep=graph.getEdgesNumber()/tasksNumber+1;
		ArrayList<Future<?>> tasks=new ArrayList<>();
		for(int i=0;i<graph.getEdgesNumber();i+=edgeStep) {
			final int from=i;
			final int to=from+edgeStep>graph.getEdgesNumber()?graph.getEdgesNumber():from+edgeStep;
			tasks.add(executor.submit(()->{
				for(int j=from;j<to;j++) {
					Graph.Edge e=graph.getEdge(j);
					EDGE_TYPE ne=edgeCopier.copy(e, (VERTEX_TYPE)mappingVertices[e.a.getIndex()], (VERTEX_TYPE)mappingVertices[e.b.getIndex()]);
					if(addEdges!=null) addEdges.add(ne);
				}
			}));
		}
		for(Future<?> t : tasks) {
			try {t.get();} catch (InterruptedException | ExecutionException e) {e.printStackTrace();}
		}
		return this;
	}
	
	
	public GraphCopier<VERTEX_TYPE> getVertices(Collection<VERTEX_TYPE> addVertices) {
		for(VERTEX_TYPE v : mappingVertices) addVertices.add((VERTEX_TYPE) v);
		return this;
	}
	
	
}
