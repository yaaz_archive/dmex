package yaaz.dmex.util;

import yaaz.dmex.Graph;


@FunctionalInterface
public interface EdgeCopier<V extends AlgorithmBase.Vertex<?,?>, E extends AlgorithmBase.Edge<?,?>> {
	
	
	public E copy(Graph.Edge edge, V a, V b);
	
	
}