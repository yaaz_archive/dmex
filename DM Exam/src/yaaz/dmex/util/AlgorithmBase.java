package yaaz.dmex.util;

import java.util.concurrent.ConcurrentLinkedQueue;
import yaaz.dmex.Graph;


public class AlgorithmBase {
	
	
	public static class Vertex<VERTEX_TYPE extends Vertex<VERTEX_TYPE, EDGE_TYPE>, EDGE_TYPE extends Edge<VERTEX_TYPE, EDGE_TYPE>> {
		public final Graph.Vertex origin;
		public final ConcurrentLinkedQueue<EDGE_TYPE> edges=new ConcurrentLinkedQueue<>();
		public Vertex(Graph.Vertex o) {
			origin=o;
		}
		public VERTEX_TYPE getNeighbor(EDGE_TYPE e) {
			if(e.a==this) return e.b;
			if(e.b==this) return e.a;
			throw new IllegalArgumentException("Edge is not connected to this vertex");
		}
	}
	public static class Edge<VERTEX_TYPE extends Vertex<VERTEX_TYPE, EDGE_TYPE>, EDGE_TYPE extends Edge<VERTEX_TYPE, EDGE_TYPE>> {
		public final Graph.Edge origin;
		public final VERTEX_TYPE a, b;
		@SuppressWarnings("unchecked")
		public Edge(Graph.Edge o, VERTEX_TYPE a, VERTEX_TYPE b) {
			origin=o;
			this.a=a;
			this.b=b;
			a.edges.add((EDGE_TYPE) this);
			b.edges.add((EDGE_TYPE) this);
		}
		public long getWeight() {
			return origin.weight;
		}
	}
	
	
}
