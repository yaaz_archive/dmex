package yaaz.dmex.util;

import yaaz.dmex.Graph;


@FunctionalInterface
public interface VertexCopier<T extends AlgorithmBase.Vertex<?,?>> {
	
	
	public T copy(Graph.Vertex edge);
	
	
}