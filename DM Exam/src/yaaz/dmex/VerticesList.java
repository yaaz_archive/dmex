package yaaz.dmex;

import java.util.ArrayList;
import yaaz.dmex.Graph.*;


class VerticesList extends ArrayList<Vertex> {
	private static final long serialVersionUID = -7098079990598176955L;
	
	
	public Vertex remove(int index) {
		Vertex v=super.remove(index);
		int s=v.index;
		v.index=-1;
		for(int i=s;i<size();i++) get(i).index--;
		for(Edge e : v.edges) e.remove();
		return v;
	}
	
	
}
