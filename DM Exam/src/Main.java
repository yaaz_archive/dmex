import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import yaaz.dmex.*;
import yaaz.dmex.Graph.Vertex;
import yaaz.dmex.boruvka.Boruvka;
import yaaz.dmex.dijkstra.Dijkstra;
import yaaz.dmex.dijkstra.PathNode;
import yaaz.dmex.Graph.Edge;
import yaaz.dmex.Graph.PointVertex;


public class Main {
	
	
	private static Canvas canvas;
	protected static Thread animationThread;
	protected static int animationSpeed=0;
	
	
	
	public static void main(String[] args) {
		Dimension screen=Toolkit.getDefaultToolkit().getScreenSize();
		JFrame frame=new JFrame("Graphs");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(screen.width/2-400, screen.height/2-300, 800, 600);
		{
			JPanel p=new JPanel();
			p.setLayout(new GridLayout());
			final JButton algo=new JButton("None");
			final JButton animation=new JButton("Animation off");
			final Runnable upd=()->{
				if(algo.getText().equals("None")) canvas.mode=null;
				else if(!animation.getText().equals("Animation off")) {
					if(algo.getText().equals("Dijkstra")) canvas.mode=DrawMode.DIJKSTRA_ANIMATED;
					else canvas.mode=DrawMode.BORUVKA_ANIMATED;
					animationSpeed=Integer.valueOf(animation.getText().split(" ")[1].replace("ms", ""));
				}
				else {
					if(algo.getText().equals("Dijkstra")) canvas.mode=DrawMode.DIJKSTRA_STATIC;
					else canvas.mode=DrawMode.BORUVKA_STATIC;
				}
				canvas.drawModeUpdate();
			};
			algo.addActionListener(e->{
				if(algo.getText().equals("None")) algo.setText("Dijkstra");
				else if(algo.getText().equals("Dijkstra")) algo.setText("Boruvka");
				else algo.setText("None");
				upd.run();
			});
			animation.addActionListener(e->{
				if(animation.getText().equals("Animation off")) animation.setText("Animation 200ms");
				else if(animation.getText().equals("Animation 200ms")) animation.setText("Animation 500ms");
				else if(animation.getText().equals("Animation 500ms")) animation.setText("Animation 1000ms");
				else if(animation.getText().equals("Animation 1000ms")) animation.setText("Animation 1500ms");
				else animation.setText("Animation off");
				upd.run();
			});
			final JButton clear=new JButton("Clear");
			clear.addActionListener(e->{
				canvas.boruvka=null;
				canvas.dijkstra=null;
				canvas.dijkstraStart=null;
				canvas.graph=new Graph();
				canvas.graphChanged();
			});
			final JButton help=new JButton("Help");
			help.addActionListener(e->new Thread(()->JOptionPane.showMessageDialog(null,
					"��� ��������� ������������� ������ ���������� ������� � ��������.\n"+
					"� ������������ ��������� ����������� ���������������, �� ��� ����� ����� ����������� ����������� �����������.\n\n"+
					"���	                            ��������/�������� �������\n"+
					"��� � ������           ��������/�������� �����\n"+
					"���                            ��������� ��������� ������� ��� ��������� ��������\n"+
					"��� � ������           ����������� �������\n\n"+
					"��������� ������ 6371\n764610@gmail.com"
			)).start());
			p.add(algo);
			p.add(animation);
			p.add(clear);
			p.add(help);
			frame.add(p, BorderLayout.NORTH);
		}
		frame.add(canvas=new Canvas(), BorderLayout.CENTER);
		frame.setVisible(true);
		
		animationThread=new Thread(()->{
			for(;;) {
				try {Thread.sleep(animationSpeed);} catch (InterruptedException e) {}
				if(canvas.mode==DrawMode.BORUVKA_ANIMATED) canvas.boruvkaNextStep();
				else if(canvas.mode==DrawMode.DIJKSTRA_ANIMATED) canvas.dijkstraNextStep();
			}
		});
		animationThread.setDaemon(true);
		animationThread.start();
		
		try(BufferedReader r=new BufferedReader(new InputStreamReader(System.in))) {
			for(;;) {
				String s=r.readLine();
				if(s.equals("get")) {
					for(Vertex v : canvas.graph.getVertices()) {
						System.out.println("Vertex v"+v.getIndex()+"=g.new IndexedVertex("+v.getIndex()+");");
					}
					for(Edge e : canvas.graph.getEdges()) {
						System.out.println("Edge e"+e.a.getIndex()+"_"+e.b.getIndex()+"=g.new Edge(v"+e.a.getIndex()+", v"+e.b.getIndex()+", "+e.weight+");");
					}
				}
				else if(s.equals("boruvka debug")) {
					canvas.mode=DrawMode.BORUVKA_DEBUG;
					canvas.drawModeUpdate();
				}
				else if(s.equals("dijkstra debug")) {
					canvas.mode=DrawMode.DIJKSTRA_DEBUG;
					canvas.drawModeUpdate();
				}
				else if(s.isEmpty()) {
					if(canvas.mode==DrawMode.BORUVKA_DEBUG) canvas.boruvkaNextStep();
					else if(canvas.mode==DrawMode.DIJKSTRA_DEBUG) canvas.dijkstraNextStep();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}

class Canvas extends JPanel implements MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 5394644166981197988L;
	protected Graph graph=new Graph();
	private int mouseX, mouseY;
	private int clickX, clickY;
	private Graph.PointVertex pressedVertex, hoveredVertex;
	private int pressed;
	protected DrawMode mode=null;
	protected Boruvka[] boruvka;
	protected Dijkstra dijkstra;
	protected Graph.Vertex dijkstraStart=null;
	public Canvas() {
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	public void paintComponent(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.GRAY);
		for(Edge eg : graph.getEdges()) {
			g.drawLine(((Graph.PointVertex)eg.a).getX(), ((Graph.PointVertex)eg.a).getY(), ((Graph.PointVertex)eg.b).getX(), ((Graph.PointVertex)eg.b).getY());
		}
		if(boruvka!=null) {
			g.setColor(Color.RED);
			for(Boruvka bk : boruvka) {
				for(Edge eg : bk.getRawResult()) {
					if(eg!=null) {
						int x1=((Graph.PointVertex)eg.a).getX(), y1=((Graph.PointVertex)eg.a).getY(), x2=((Graph.PointVertex)eg.b).getX(), y2=((Graph.PointVertex)eg.b).getY();
						drawBoldLine(g, x1,y1,x2,y2);
					}
				}
			}
		}
		else if(dijkstra!=null) {
			g.setColor(Color.RED);
			for(PathNode nn : dijkstra.getRawResult()) {
				if(nn!=null) {
					if(nn.vertex==dijkstraStart) {
						Graph.PointVertex v=(Graph.PointVertex) dijkstraStart;
						int rad=v==hoveredVertex?12:7;
						g.fillOval(v.getX()-rad, v.getY()-rad, rad*2, rad*2);
					}
					if(nn.path!=null) {
						Graph.PointVertex v=(PointVertex) nn.vertex;
						Graph.PointVertex n=(PointVertex) nn.getNextNode();
						double dx=v.getX()-n.getX(), dy=v.getY()-n.getY(), len=Math.sqrt(dx*dx+dy*dy);
						dx/=len;dy/=len;
						double x=n.getX()+dx*5, y=n.getY()+dy*5;
						drawBoldLine(g, v.getX(), v.getY(), n.getX(), n.getY());
						drawBoldLine(g, (int)x,(int)y,(int)(x+dx*20+dy*5),(int)(y+dy*20-dx*5));
						drawBoldLine(g, (int)x,(int)y,(int)(x+dx*20-dy*5),(int)(y+dy*20+dx*5));
					}
				}
			}
			g.setXORMode(Color.GREEN);
			for(PathNode nn : dijkstra.getRawResult()) {
				if(nn!=null&&nn.path!=null) {
					Graph.PointVertex v=(PointVertex) nn.vertex;
					g.drawString(String.valueOf(nn.pathLength), v.getX()+10, v.getY());
				}
			}
			g.setPaintMode();
		}
		g.setColor(Color.BLACK);
		for(Vertex av : graph.getVertices()) {
			Graph.PointVertex v=(Graph.PointVertex) av;
			int rad=v==hoveredVertex?10:5;
			g.fillOval(v.getX()-rad, v.getY()-rad, rad*2, rad*2);
		}
		for(Edge eg : graph.getEdges()) {
			int x=(((Graph.PointVertex)eg.a).getX()+((Graph.PointVertex)eg.b).getX())/2;
			int y=(((Graph.PointVertex)eg.a).getY()+((Graph.PointVertex)eg.b).getY())/2;
			g.drawString(String.valueOf(eg.weight), x, y);
		}
		if(pressedVertex!=null) {
			if(pressed!=0) g.drawLine(pressedVertex.getX(), pressedVertex.getY(), mouseX, mouseY);
			if(pressed==2) {
				g.drawLine(pressedVertex.getX()+5, pressedVertex.getY(), mouseX+5, mouseY);
				g.drawLine(pressedVertex.getX(), pressedVertex.getY()+5, mouseX, mouseY+5);
				g.drawLine(pressedVertex.getX()-5, pressedVertex.getY(), mouseX-5, mouseY);
				g.drawLine(pressedVertex.getX(), pressedVertex.getY()-5, mouseX, mouseY-5);
				g.fillOval(mouseX-5, mouseY-5, 10, 10);
			}
		}
	}
	private void drawBoldLine(Graphics g, int x1, int y1, int x2, int y2) {
		g.drawLine(x1,y1,x2,y2);
		g.drawLine(x1+1,y1,x2+1,y2);
		g.drawLine(x1,y1+1,x2,y2+1);
		g.drawLine(x1-1,y1,x2-1,y2);
		g.drawLine(x1,y1-1,x2,y2-1);
	}
	public void mouseClicked(MouseEvent e) {updateMouse(e);}
	public void mousePressed(MouseEvent e) {
		clickX=e.getX();
		clickY=e.getY();
		pressedVertex=getVertex(e.getX(), e.getY());
		pressed=e.getButton()==MouseEvent.BUTTON1?1:2;
		this.repaint();
	}
	public void mouseReleased(MouseEvent e) {
		if((Math.pow(e.getX()-clickX, 2)+Math.pow(e.getY()-clickY, 2))<=25) {
			if(pressed==1) {
				Graph.PointVertex del=getVertex(e.getX(), e.getY());
				if(del==null) graph.new PointVertex(e.getX(), e.getY());
				else del.remove();
			}
			else dijkstraStart=getVertex(e.getX(), e.getY());
			graphChanged();
		}
		else if(pressedVertex!=null) {
			if(pressed==1) {
				if(hoveredVertex!=null) {
					Edge eg=pressedVertex.getEdge(hoveredVertex);
					if(eg==null) graph.new Edge(pressedVertex, hoveredVertex, pressedVertex.getDistanceTo(hoveredVertex)/20);
					else eg.remove();
				}
				else {
					Graph.PointVertex v=graph.new PointVertex(e.getX(), e.getY());
					graph.new Edge(pressedVertex, v, pressedVertex.getDistanceTo(v)/20);
				}
			}
			else {
				Graph.PointVertex v=graph.new PointVertex(e.getX(), e.getY());
				for(Graph.Vertex tv : pressedVertex.getNeighbors()) {
					Graph.PointVertex t=(PointVertex) tv;
					graph.new Edge(v, t, v.getDistanceTo(t)/20);
				}
				pressedVertex.remove();
			}
			graphChanged();
		}
		pressed=0;
	}
	public void mouseEntered(MouseEvent e) {updateMouse(e);}
	public void mouseExited(MouseEvent e) {updateMouse(e);}
	public void mouseDragged(MouseEvent e) {updateMouse(e);}
	public void mouseMoved(MouseEvent e) {updateMouse(e);}
	private void updateMouse(MouseEvent e) {
		mouseX=e.getX();
		mouseY=e.getY();
		hoveredVertex=getVertex(e.getX(), e.getY());
		this.repaint();
	}
	protected void drawModeUpdate() {
		if(mode==null) {
			dijkstra=null;
			boruvka=null;
			this.repaint();
			return;
		}
		boolean stat=false;
		switch(mode) {
		case BORUVKA_STATIC:
			stat=true;
		case BORUVKA_ANIMATED:
		case BORUVKA_DEBUG:
			dijkstra=null;
			Graph[] gs=graph.splitIntoConnected();
			Boruvka[] bs=new Boruvka[gs.length];
			for(int i=0;i<gs.length;i++) {
				Boruvka b=new Boruvka(gs[i]);
				if(stat) b.execute();
				bs[i]=b;
			}
			boruvka=bs;
			break;
		case DIJKSTRA_STATIC:
			stat=true;
		case DIJKSTRA_ANIMATED:
		case DIJKSTRA_DEBUG:
			boruvka=null;
			if(dijkstraStart!=null&&!dijkstraStart.isRemoved()) {
				Dijkstra d=new Dijkstra(graph, dijkstraStart.getIndex());
				if(stat) d.execute();
				dijkstra=d;
			}
			else dijkstra=null;
			break;
		}
		this.repaint();
	}
	protected void graphChanged() {
		drawModeUpdate();
		Main.animationThread.interrupt();
	}
	protected void boruvkaNextStep() {
		found:if(boruvka!=null) {
			for(Boruvka bk : boruvka) {
				if(bk.step()) break found;
			}
			drawModeUpdate();
		}
		repaint();
	}
	protected void dijkstraNextStep() {
		if(dijkstra!=null&&!dijkstra.step()) drawModeUpdate();
		repaint();
	}
	private Graph.PointVertex getVertex(int x, int y) {
		return getVertexInRange(x, y, 20);
	}
	private Graph.PointVertex getVertexInRange(int x, int y, int range) {
		Graph.PointVertex v=getClosestVertex(x, y);
		if(v==null) return null;
		double l=Math.sqrt(Math.pow(x-v.getX(), 2)+Math.pow(y-v.getY(), 2));
		return l<=range?v:null;
	}
	private Graph.PointVertex getClosestVertex(int x, int y) {
		Graph.PointVertex v=null;
		double len=0;
		for(Vertex av : graph.getVertices()) {
			Graph.PointVertex tv=(Graph.PointVertex) av;
			double l=Math.sqrt(Math.pow(x-tv.getX(), 2)+Math.pow(y-tv.getY(), 2));
			if(l<len||v==null) {
				len=l;
				v=tv;
			}
		}
		return v;
	}
}





enum DrawMode {
	BORUVKA_STATIC,
	BORUVKA_ANIMATED,
	BORUVKA_DEBUG,
	DIJKSTRA_STATIC,
	DIJKSTRA_ANIMATED,
	DIJKSTRA_DEBUG;
}